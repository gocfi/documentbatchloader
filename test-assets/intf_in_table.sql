DROP TABLE intf_in_Document;

-- Auto generated script for ORACLE.
CREATE TABLE intf_in_Document (
	IMD_ID INTEGER NOT NULL,
	IMD_STATUS VARCHAR2(1000)  DEFAULT 'Ready',
	IMD_MESSAGE VARCHAR2(1000) ,
	TRIRIGA_RECORD_ID VARCHAR2(1000) ,
	NAME VARCHAR2(1000) ,
	DESCRIPTION VARCHAR2(1000) ,
	REPORTFILE VARCHAR2(1000) ,
	TRIDOCUMENTTYPECL VARCHAR2(1000) ,
	DOC_BLOB BLOB
);

-- It is recommended to create a database Sequence and apply a Trigger to the IMD_ID column
-- for each table created to automatically increment the value as rows are inserted. 
-- Here is a sample Trigger create script for this table using the Sequence name in the sample script below.
CREATE OR REPLACE TRIGGER intf_in_Document_trigger
 BEFORE INSERT ON intf_in_Document FOR EACH ROW
 begin
  select IMD_ID_SEQ.nextval into :new.imd_id from dual;
end;

-- Here is a sample Sequence create script. Only run once.
CREATE SEQUENCE IMD_ID_SEQ
 START WITH 1
 MAXVALUE 18446744073709551615
 MINVALUE 1
 NOCYCLE
 NOCACHE
 NOORDER;
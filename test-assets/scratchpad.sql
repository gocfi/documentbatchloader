
CREATE DIRECTORY PETCO_DOCFILES AS '/vagrant/PETCODEV/documents/docfiles';
grant read, write on directory PETCO_DOCFILES to TRIDATA;

SELECT  * FROM CFI_DOCBATCHLDR_IN;
TRUNCATE TABLE CFI_DOCBATCHLDR_IN;
INSERT INTO CFI_DOCBATCHLDR_IN  ( FILENAME, DOC_TITLE) VALUES ('9761.pptx', 'Special Presentation.ppt');
INSERT INTO CFI_DOCBATCHLDR_IN  ( FILENAME, DOC_TITLE) VALUES ('9762.pptx', 'Special Presentation2.ppt');
INSERT INTO CFI_DOCBATCHLDR_IN  ( FILENAME, DOC_TITLE) VALUES ('9763.pptx', 'Special Presentation3.ppt');
INSERT INTO CFI_DOCBATCHLDR_IN  ( FILENAME, DOC_TITLE) VALUES ('9911.docx', 'Word Doc.doc');
INSERT INTO CFI_DOCBATCHLDR_IN  ( FILENAME, DOC_TITLE) VALUES ('does not exist.txt', 'Word Doc.doc');
INSERT INTO CFI_DOCBATCHLDR_IN  ( FILENAME, DOC_TITLE) VALUES ('test.png', 'test.png');
INSERT INTO CFI_DOCBATCHLDR_IN  ( FILENAME, DOC_TITLE) VALUES ('pdf_tri_connector.pdf', 'pdf_tri_connector.pdf');
COMMIT;


SET SERVEROUTPUT ON;
begin
 documentbatchloader.loaddocs('PETCO_DOCFILES');
end;
/

select * from intf_in_Document order by imd_id desc;

TRUNCATE table intf_in_Document;

update intf_in_Document
set TRIDOCUMENTTYPECL='Amendment';
commit;

select tridocumenttypecl, sys_name1, sys_description, dm_path, dm_document_number, system_file_path recordname, tripathsy, trirecordnamesy from t_document
where spec_id >= 15730260
order by spec_id desc;

UPDATE ibs_spec
   SET object_state = NULL, updated_date = sysdate-31, object_id = spec_id*-1
 WHERE spec_id IN (
            select spec_id from t_document
            where spec_id > 15730260
        );
        
        
SELECT * From DM_CONTENT;

UPDATE DM_CONTENT
set MIME_TYPE ='application/vnd.openxmlformats-officedocument.presentationml.presentation'
WHERE FILE_NAME like '%.pptx';
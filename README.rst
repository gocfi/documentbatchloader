Document Batch Loader
=====================
The TRIRIGA_ application stores documents as BLOBS in the database. This makes
the process of loading new documents somewhat complicated. When you have
thousands of documents manually uploading them one by one becomes an impossible
task.

There are a few options to batch load the documents. You can use TRIRIGA's SOAP
API BusinessConnect_. You will have to use Java or another language with a SOAP
client to use this. Depending on the size of the documents you are uploading,
the performance may suffer with this method. With BusinessConnect, the documents
are either sent to TRIRIGA through the SOAP request then uploaded to the database.
The extra hops will certainly slow things down.

Here, we used PL/SQL and the new Integration Object feature to upload the
documents. While the documents are still copied around in a staging table and
the TRIRIGA data table the performance is much better since everything is
running on the database server.

The process works like this:

1. Information about the document you want to load (file name and title) are
   inserted into a table.
2. The actual documents are uploaded to a directory accessible to the database.
3. A PL/SQL procedure is called to load the files into a database BLOB column.
   The procedure also stages the records for upload via *Integration Object*.
4. The Integration Object is executed and the documents are uploaded.

.. _TRIRIGA: http://www-03.ibm.com/software/products/en/ibmtrir
.. _BusinessConnect: http://www-01.ibm.com/support/knowledgecenter/SSHEB3_3.4.1/com.ibm.tap.doc/con_cba/c_cba_overview.html

This was tested to work in TRIRIGA 3.4/10.3 and 3.3.2/10.3.2 releases.

You can find the project and source at https://bitbucket.org/gocfi/documentbatchloader

Using the Integration
---------------------
1. Open ``TRIRIGAWEB.properties``. Make sure the value of
   ``ENFORCE_REQUIRED_VALIDATION`` setting is ``N``. If not, change the value
   and restart TRIRIGA to apply.

   If the setting is not set to ``N``, you will receive an error about
   ``triNameTX`` on permissions being required.

2. Load the OM package with the Integration object: ``integration-object/documentbatchloader-IntObj.zip``

3. Open the Integration Object (Tools > System Setup > Integration >
   Integration Object). The name of our configuration is *CFI - DB - IN -
   Document - Document - Document - Upload*

4. Click on the Locator button for the *Datasource Name* field to select a data
   source. If you do not have a datasource setup, use the Add link in the popup
   to create one. Use the built-in example data sources as a guide.

   Once a datasource is selected, use the *Test DB Connection* action to test it.

5. Click on the *Data Map* tab in the Integration Object and find the *Base
   Parent* field. This is the folder where these document will be uploaded to
   and it's set to ``\ROOT\Object Attachments``.  If you would like to use
   another folder, change it accordingly.

6. Create the integration object *in* table (you can get an updated version by
   clicking on Generate SQL in the integration object)

   .. code:: sql

        DROP TABLE intf_in_Document;

        -- Auto generated script for ORACLE.
        CREATE TABLE intf_in_Document (
            IMD_ID INTEGER NOT NULL,
            IMD_STATUS VARCHAR2(1000)  DEFAULT 'Ready',
            IMD_MESSAGE VARCHAR2(1000) ,
            TRIRIGA_RECORD_ID VARCHAR2(1000) ,
            NAME VARCHAR2(1000) ,
            DESCRIPTION VARCHAR2(1000) ,
            REPORTFILE VARCHAR2(1000) ,
            TRIDOCUMENTTYPECL VARCHAR2(1000) ,
            DOC_BLOB BLOB
        );

        -- It is recommended to create a database Sequence and apply a Trigger to the IMD_ID column
        -- for each table created to automatically increment the value as rows are inserted.
        -- Here is a sample Trigger create script for this table using the Sequence name in the sample script below.
        CREATE OR REPLACE TRIGGER intf_in_Document_trigger
        BEFORE INSERT ON intf_in_Document FOR EACH ROW
        begin
        select IMD_ID_SEQ.nextval into :new.imd_id from dual;
        end;

        -- Here is a sample Sequence create script. Only run once.
        CREATE SEQUENCE IMD_ID_SEQ
        START WITH 1
        MAXVALUE 18446744073709551615
        MINVALUE 1
        NOCYCLE
        NOCACHE
        NOORDER;

7. Create a directory object to where your files are located:

   .. code:: sql

        CREATE DIRECTORY CFI_DOCS AS '/ora_exp/CFIDOCSDB/documents';
        grant read, write on directory CFI_DOCS to TRIDATA;

8. Load the list of document to the ``CFI_DOCBATCHLDR_IN`` table.

   .. code:: sql

        TRUNCATE TABLE CFI_DOCBATCHLDR_IN;
        INSERT INTO CFI_DOCBATCHLDR_IN  ( FILENAME, DOC_TITLE) VALUES ('9761.pptx', 'Special Presentation');
        INSERT INTO CFI_DOCBATCHLDR_IN  ( FILENAME, DOC_TITLE) VALUES ('9762.pptx', 'Another Presentation');
        INSERT INTO CFI_DOCBATCHLDR_IN  ( FILENAME, DOC_TITLE) VALUES ('9763.pptx', 'One more Presentation');
        COMMIT;

   ``FILENAME`` is the actual file name on disk. ``DOC_TITLE`` is the title of
   the document. DOC_TITLE becomes the filename in TRIRIGA.

9. Run the procedure.

   .. code:: sql

        set serveroutput on;
        begin
            documentbatchloader.loaddocs('PETCO_DOCFILES');
        end;
        /

   This will:

   1. Iterate the ``CFI_DOCBATCHLDR_IN`` table
   2. For each row:

      1. Checks if the file mentioned in the ``FILENAME`` column exists
      2. If it does, inserts the row into ``intf_in_Document``
      3. Read the file into the Blob column in ``intf_in_Document``.


10. Check if the documents were staged:

    .. code:: sql

        select * from intf_in_Document order by imd_id desc;

11. Execute the integration object::

      curl 'http://server:8001/html/en/default/rest/Integration?USERNAME=system&PASSWORD=admin&ioName=CFI+-+DB+-+IN+-+Document+-+Document+-+Document+-+Upload'

12. Check for the newly loaded documents:

    .. code:: sql

        SELECT
            tridocumenttypecl,
            sys_name1,
            sys_description,
            dm_path,
            dm_document_number,
            system_file_path recordname,
            tripathsy,
            trirecordnamesy
        FROM
            t_document
        WHERE
            spec_id >= 15730260
        ORDER BY
            spec_id DESC;

13. You can also log into TRIRIGA to verify that the documents are correctly
    uploaded.

Deploying the code
-------------------
This project is setup to be compiled and deployed via Maven.

1. Edit your Maven settings file (``HOME/.m2/Settings.xml``) and add this:

   Edit the settings to match the connection info for your database.

   .. code:: xml

    <settings>
        <profiles>
            <profile>
                <id>dev_vm</id>
                <properties>
                    <databaseUrl>jdbc:oracle:thin:@192.168.0.179:1521:xe</databaseUrl>
                    <databaseUser>tridata</databaseUser>
                    <databasePassword>tridata</databasePassword>
                </properties>
            </profile>
        </profiles>
    </settings>

2. Download ``ojdbc6.jar`` file from the Oracle website and install it to your local Maven repo::

    mvn install:install-file -DgroupId=com.oracle -DartifactId=ojdbc6 -Dversion=11.2.0.2.0 -Dpackaging=jar -Dfile=ojdbc6.jar

3. Compile and deploy the project to your database::

    mvn -Pdev_vm clean compile

   Notice that the part after ``-P`` matches the profile name from step 1.

Repeat step 3 any time you change the PL/SQL code to redeploy your project.

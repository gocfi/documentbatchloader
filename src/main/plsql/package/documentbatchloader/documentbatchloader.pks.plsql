create or replace package documentbatchloader
as

    function loadfiletoblob(dir_obj_name in varchar,
                            file_name in varchar,
                            blob_pointer in out BLOB) return boolean;

    function file_exists(dir_obj_name in varchar,
                         file_name in varchar) return boolean;

    procedure loaddocs(dir_obj_name in varchar);

end documentbatchloader;
/


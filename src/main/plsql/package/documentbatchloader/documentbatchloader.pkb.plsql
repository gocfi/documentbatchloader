create or replace package body documentbatchloader
as

    function file_exists(dir_obj_name in varchar,
                         file_name in varchar) return boolean
    /***************************************************************
     * Checks if a file exists in the given directory.
     *
     * Params:
     *  dir_obj_name: The name of the directory object (VARCHAR)
     *  file_name: The name of the file (VARCHAR)
     *
     * Returns:
     *  True if file exists, otherwise False.
     ***************************************************************/
    as
        bfile_pointer bfile;
        bfile_exists number;
    begin
        bfile_pointer := bfilename(upper(dir_obj_name), file_name);
        bfile_exists := dbms_lob.fileexists(bfile_pointer);
        dbms_lob.fileclose(bfile_pointer);

        if bfile_exists = 1
        then
            return true;
        else
            return false;
        end if;
    end;


    function loadfiletoblob(dir_obj_name in varchar,
                            file_name in varchar,
                            blob_pointer in out blob) return boolean
    /***************************************************************
     * Reads a file into the given BLOB locator.
     *
     * Params:
     *  dir_obj_name: The name of the directory object (VARCHAR)
     *  file_name: The name of the file (VARCHAR)
     *  blob_pointer: The locator to a BLOB field. When
     *                selecting this from a row, make sure the row
     *                is locked (BLOB)
     *
     * Returns:
     *  True if file was loaded, otherwise False.
     ***************************************************************/
     as
        bfile_pointer bfile;
        bfile_offset number :=1;
        blob_offset number :=1;
        tot_len integer;
    begin
        bfile_pointer := bfilename(upper(dir_obj_name), file_name);

        IF dbms_lob.fileexists(bfile_pointer) = 1
        THEN
            dbms_lob.fileopen(bfile_pointer,dbms_lob.file_readonly);
            dbms_lob.OPEN(blob_pointer, dbms_lob.lob_readwrite);
            dbms_lob.loadblobfromfile(blob_pointer,bfile_pointer, dbms_lob.lobmaxsize, bfile_offset, blob_offset);
            tot_len := dbms_lob.getlength(blob_pointer);
            dbms_lob.close(blob_pointer);
            dbms_lob.fileclose(bfile_pointer);

            dbms_output.put_line('Read  ' || to_char(tot_len) || ' bytes from "' || file_name || '" into BLOB.');
            return true;
        ELSE
            return false;
        END IF;
    end;


    procedure loaddocs(dir_obj_name in varchar)
    /***************************************************************
     * Stages documents for import into TRIRIGA.
     *
     * Params:
     *  dir_obj_name: The name of the directory object (VARCHAR)
     *
     ***************************************************************/
    as
        blob_pointer blob;
        load_blob_status boolean;

        cursor files_cur is
            select filename, doc_title
            from cfi_docbatchldr_in;
    begin
        for file_rec in files_cur
        loop
            if file_exists(dir_obj_name, file_rec.filename)
            then
                insert into intf_in_document
                (
                    name,
                    description,
                    reportfile,
                    tridocumenttypecl,
                    doc_blob
                )
                values
                (
                    file_rec.DOC_TITLE,
                    '',
                    'FALSE',
                    'Amendment',
                    empty_blob()
                ) returning doc_blob into blob_pointer;

                load_blob_status := loadfiletoblob(dir_obj_name, file_rec.filename, blob_pointer);

                dbms_output.put_line('Loaded "' || file_rec.filename || '".');
            else
                dbms_output.put_line('Not loading "' || file_rec.filename || '" because it was not found in the directory "' || dir_obj_name || '".' );
            end if;
        end loop;

        commit;

    end;

end documentbatchloader;
/

